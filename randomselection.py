#!/usr/bin/env python

import sys
import csv
import json
import hashlib
import random

def main(argv):
    duplicate_checker_list = [] 
    new_list = {}
    with open(argv[0]) as f:
        for line in csv.DictReader(f, fieldnames=('Email Address', 'BNB Address', 'Telegram ID', 'ELA Address')):
            email_address = line['Email Address']
            email_address_hash = hashlib.md5(email_address).hexdigest()
            ela_address = line['ELA Address']
            ela_address_hash = hashlib.md5(ela_address).hexdigest()
            telegram_id = line['Telegram ID']      
            telegram_id_hash = hashlib.md5(telegram_id).hexdigest()
            if email_address_hash not in duplicate_checker_list and ela_address_hash not in duplicate_checker_list and telegram_id_hash not in duplicate_checker_list:
                duplicate_checker_list.append(email_address_hash)
                duplicate_checker_list.append(ela_address_hash)
                duplicate_checker_list.append(telegram_id_hash)
                new_list[email_address_hash] = "{0},{1},{2}".format(email_address, telegram_id, ela_address)
    
    del duplicate_checker_list
   
    new_list_keys = new_list.keys() 
    with open(argv[1], 'wb') as out:
        num_winners = 1
        out.write("{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "200 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 2
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "100 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 4
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "50 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 10
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "10 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 20
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "2 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 1000
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, new_list_keys, "0.5 ELA", num_winners):
            out.write("{0}\n".format(winner))
 
def select_winners(new_list, new_list_keys, winner_type, num_winners):
    result = []
    for i in range(num_winners):
        random.shuffle(new_list_keys)
        winner = random.choice(new_list_keys)
        result.append(new_list[winner])
        new_list_keys.remove(winner)
        del new_list[winner]
    return result

if __name__ == '__main__':
    main(sys.argv[1:])
